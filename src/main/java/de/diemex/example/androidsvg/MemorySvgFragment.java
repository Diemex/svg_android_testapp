package de.diemex.example.androidsvg;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.elements.SvgRect;
import com.caverock.androidsvg.elements.SvgTag;

import org.xml.sax.SAXException;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class MemorySvgFragment extends RoboFragment
{
    @InjectView(R.id.img_memory_svg) ImageView mImageView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.frag_memory_svg, null);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        //Construct our svg
        SVG svgDocument = new SVG();
        SvgTag svgRoot = new SvgTag(svgDocument, null);
        SvgRect svgRect = new SvgRect(svgDocument, svgRoot);
        try
        {
            svgRoot.addChild(svgRect);
        } catch (SAXException e)
        {
            e.printStackTrace();
        }
        //TODO well we can't do much here......
    }
}
