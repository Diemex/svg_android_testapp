package de.diemex.example.androidsvg;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IdProvider
{
    private static Map<String, Integer> mRawSvgs = new HashMap<String, Integer>();
    private static Map<String, Integer> mDrawableIds = new HashMap<String, Integer>();
    private static List<String> mFailingTests = new ArrayList<String>();
    private static List<String> mSortedTitles;


    static
    {
        initRmFailingTests();
        initSvgs();
        initPngs();
        initTitles();
    }


    public static Map<String, Integer> getRawSvgs()
    {
        return mRawSvgs;
    }


    public static Map<String, Integer> getDrawableIds()
    {
        return mDrawableIds;
    }


    public static List<String> getFailingTestTitles()
    {
        return mFailingTests;
    }


    public static List<String> getSortedTitles()
    {
        return mSortedTitles;
    }


    public static int svgIdForName(String name)
    {
        return mRawSvgs.get(name);
    }


    public static int svgIdForPos(int pos)
    {
        return svgIdForName(mSortedTitles.get(pos));
    }


    private static void initSvgs()
    {
        Field[] fields = R.raw.class.getFields();
        for (Field f : fields)
            try
            {
                mRawSvgs.put(f.getName(), f.getInt(null));
            } catch (IllegalArgumentException ignored)
            {
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
    }


    private static void initPngs()
    {
        Field[] fields = R.drawable.class.getFields();
        for (Field f : fields)
            try
            {
                //Drawable class also has all the android icons..., filter out only svgs
                if (mRawSvgs.containsKey(f.getName()))
                    mDrawableIds.put(f.getName(), f.getInt(null));
            } catch (IllegalArgumentException ignored)
            {
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
    }


    private static void initTitles()
    {
        mSortedTitles = new ArrayList<String>(mDrawableIds.size());
        mSortedTitles.addAll(mDrawableIds.keySet());
        mSortedTitles.removeAll(mFailingTests);
        Collections.sort(mSortedTitles);
    }


    private static void initRmFailingTests()
    {
        mFailingTests.clear();
        mFailingTests.add("color_prop_02_f");
//        mFailingTests.add("animate_struct_dom_01_b"); //Not failing
        mFailingTests.add("struct_use_12_f");
        mFailingTests.add("text_tref_02_b");
    }
}
