package de.diemex.example.androidsvg;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class ListAllFragment extends RoboFragment
{
    private ListAdapter mListAdapter;
    @InjectView(R.id.listview_svgs) private ListView mListView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.frag_list_all, null);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        mListAdapter = new ListAdapter(getActivity());
        mListView.setAdapter(mListAdapter);
    }
}
