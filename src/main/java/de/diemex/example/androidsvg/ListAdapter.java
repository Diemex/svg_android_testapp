package de.diemex.example.androidsvg;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.caverock.androidsvg.SVGImageView;

import java.util.List;
import java.util.Map;

public class ListAdapter extends BaseAdapter
{
    private Activity mContext;
    private Map<String, Integer> mDrawableIdMap;
    private Map<String, Integer> mSvgIdMap;
    private List<String> mSortedTitles;
    private LayoutInflater mLayoutInflater = null;


    //Doesn't support changing of dataset
    public ListAdapter(Activity context)
    {
        mContext = context;
        mDrawableIdMap = IdProvider.getDrawableIds();
        mSvgIdMap = IdProvider.getRawSvgs();
        mSortedTitles = IdProvider.getSortedTitles();
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount()
    {
        return mSortedTitles.size();
    }


    @Override
    public Object getItem(int pos)
    {
        return mDrawableIdMap.get(pos);
    }


    @Override
    public long getItemId(int position)
    {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        ViewHolder viewHolder;
        if (convertView == null)
        {
            v = mLayoutInflater.inflate(R.layout.list_item, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        } else
        {
            viewHolder = (ViewHolder) v.getTag();
        }
        viewHolder.txtTitle.setText(mSortedTitles.get(position));
        viewHolder.pngImg.setImageResource(mDrawableIdMap.get(mSortedTitles.get(position)));
        viewHolder.svgImage.setImageResource(mSvgIdMap.get(mSortedTitles.get(position)));
        return v;
    }


    class ViewHolder
    {
        public ImageView pngImg;
        public SVGImageView svgImage;
        public TextView txtTitle;


        public ViewHolder(View base)
        {
            pngImg = (ImageView) base.findViewById(R.id.img_png_expected);
            svgImage = (SVGImageView) base.findViewById(R.id.img_svg_result);
            txtTitle = (TextView) base.findViewById(R.id.txt_title_single);
        }
    }
}
