package de.diemex.example.androidsvg;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class TestAllFragment extends RoboFragment implements View.OnClickListener, ICallback, AdapterView.OnItemSelectedListener
{
    @InjectView(R.id.btn_test_all) private Button mBtnAll;
    @InjectView(R.id.was_fuer_ein_spinner_passing) Spinner mSpinnerPassing;
    @InjectView(R.id.was_fuer_ein_spinner_failing) Spinner mSpinnerFailing;
    @InjectView(R.id.img_view_display_single) private ImageView mImgSvg;
    @InjectView(R.id.txt_title_single) private TextView mTxtTitle;
    int mCurrentlyDisplayedId = -1;
    int mCurrentPos = 0;
    boolean mContinuousLoading = false;
    private boolean firstStart = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.frag_choose, null);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        mSpinnerPassing.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, IdProvider.getSortedTitles()));
        mSpinnerPassing.setOnItemSelectedListener(this);
        mSpinnerFailing.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, IdProvider.getFailingTestTitles()));
        mSpinnerFailing.setOnItemSelectedListener(this);
        mBtnAll.setOnClickListener(this);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_test_all:
                startLoadingAll();
                break;
        }
    }


    public void startLoadingAll()
    {
        if (IdProvider.getSortedTitles().size() >= mCurrentPos)
            mCurrentPos = 0;
        mCurrentlyDisplayedId = IdProvider.svgIdForPos(mCurrentPos);
        mContinuousLoading = true;
        loadSingle(mCurrentlyDisplayedId);
    }


    public void loadSingle(int id)
    {
        mCurrentlyDisplayedId = id;
        new LoadSvgTask(getActivity(), this, mImgSvg).execute(id);
    }


    @Override
    public void callbackOnFinish()
    {
        mTxtTitle.setText(IdProvider.getSortedTitles().get(mCurrentPos));
        mCurrentPos++;
        if (mCurrentPos >= IdProvider.getSortedTitles().size())
        {
            //Reset so we can start from the beginning
            mCurrentPos = 0;
            mContinuousLoading = false;
        } else if (mContinuousLoading)//Walk through the pictures alphabetically
            loadSingle(IdProvider.svgIdForPos(mCurrentPos));
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if (parent.equals(mSpinnerPassing))
        {
            loadSingle(IdProvider.svgIdForPos(position));
        } else if (parent.equals(mSpinnerFailing) &&! firstStart)
        {
            loadSingle(IdProvider.getRawSvgs().get(
                    IdProvider.getFailingTestTitles().get(position)
            ));
        }
        else //quickfix for app crashing because its trying to load a failing test
            firstStart = false;
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {
        //do nothing
    }
}
