package de.diemex.example.androidsvg;

import android.content.Context;
import android.graphics.Picture;
import android.graphics.drawable.PictureDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.lang.ref.WeakReference;

public class LoadSvgTask extends AsyncTask<Integer, Integer, Picture>
{
    private final Context mContext;
    private final WeakReference<ImageView> mImageViewReference;
    private final ICallback mCallback;


    public LoadSvgTask(Context context, ICallback callback, ImageView imageView)
    {
        this.mContext = context;
        this.mCallback = callback;
        mImageViewReference = new WeakReference<ImageView>(imageView);
    }


    protected Picture doInBackground(Integer... resourceId)
    {
        try
        {
            SVG svg = SVG.getFromResource(mContext, resourceId[0]);
            return svg.renderToPicture();
        } catch (SVGParseException e)
        {
            Log.e("LoadSvgTask", String.format("Error loading resource 0x%d: %s", resourceId[0], e.getMessage()));
        }
        return null;
    }


    protected void onPostExecute(Picture picture)
    {
        ImageView imgView = mImageViewReference.get();
        if (imgView != null && picture != null && !isCancelled())
        {
            imgView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            imgView.setImageDrawable(new PictureDrawable(picture));
            imgView.refreshDrawableState();
        }
        mCallback.callbackOnFinish();
    }
}
